// Config de eslint, eslint vérifie si le code JS ne comporte pas d'erreur d'écriture ni d'erreur de style.
// lien doc(https://eslint.org/docs/rules/)
module.exports = {
    // Utilise les règles de airbnb (lien: https://github.com/airbnb/javascript)
    "extends": "airbnb-base",
    // surcharge des regles airbnb
    "rules": {
        // ne tien pas en compte des règles d'indentation du code d'airbnb
       "indent": 0
    }
}
