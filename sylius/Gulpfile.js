var gulp = require('gulp');
var chug = require('gulp-chug');
var argv = require('yargs').argv;
var webpack = require('gulp-webpack');
var path = require('path');

config = [
    '--rootPath',
    argv.rootPath || '../../../../../../../web/assets/',
    '--nodeModulesPath',
    argv.nodeModulesPath || '../../../../../../../node_modules/'
];

gulp.task('admin', function() {
    gulp.src('vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Gulpfile.js', { read: false })
        .pipe(chug({ args: config }))
    ;
});

gulp.task('shop', function() {
    gulp.src('vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Gulpfile.js', { read: false })
        .pipe(chug({ args: config }))
    ;
});

// gulp.task('webpack', function () {
//     return gulp.src('dist/init.js')
//         .pipe(webpack())
//         .pipe(gulp.dest('./dist'));
// });

gulp.task('default', ['admin', 'shop']);
