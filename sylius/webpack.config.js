const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const webpack = require('webpack');

// Si le script npm qui lance webpack a l'argument dev retourne true sinon false
const dev = process.env.NODE_ENV === "dev";

const entryThemePath = './web/assets/crimson-theme';

// Ensemble de loader commun pour le css et scss
let cssLoader = [
    {
        // Interpretre les imports et les url() dans le css ou le scss.
        //lien doc: https://webpack.js.org/loaders/css-loader/
        loader: 'css-loader',

        options: {
            // Charge le css-loader en premier
            importLoaders: 1,
            // Minimise le code css ou scss si on est pas en dev
            minimize: !dev ? true : false
        }
    },
    {
        // postcss loader pour webpack
        // Plus d'info sur le postcss: http://postcss.org/
        // Info sur le loader: https://webpack.js.org/loaders/postcss-loader/
        loader: 'postcss-loader',
        options: {
            ident: 'postcss',
            plugins: (loader) => [
                // Ajoute les prefixes css pour que celui-ci soit compatible avec les diffrents navigateurs
                // https://github.com/postcss/autoprefixer
                require('autoprefixer')({
                    browsers: ['last 2 versions', 'ie > 9']
                }),
            ]
        }
    }
];

// Config de webpack. Lien doc: https://webpack.js.org/configuration/
let config = {
    // Définirs les fichiers que webpack va transformer
    // Lien doc: https://webpack.js.org/configuration/entry-context/#entry
    entry: {
        app: ['./front/js/init.js', './front/scss/style.scss']
    },
    // Webpack regarde les changements de fichiers uniquement en mode dev
    // Lien doc: https://webpack.js.org/configuration/watch/#watch
    watch: dev,
    // Configuration des fichiers de sortie
    // Lien doc: https://webpack.js.org/configuration/output/
    output: {
        // Dossier où webpack va mettre les fichiers compilers
        // l'option path n'accepte que des chemins absolues la fonction path.resolve (qui est un module node.js) sert à crée un chemin absolue (ici c'est sur le dossier dist)
        // lien doc: https://webpack.js.org/configuration/output/#output-path
        path: path.resolve(entryThemePath),
        // Nom des fichiers compiler [name] sert à dire à webpack de prendre le meme nom de fichier que le fichier d'entrée
        // lien doc: https://webpack.js.org/configuration/output/#output-filename
        filename: 'app.[hash].js',
        // Url
        publicPath: path.join(entryThemePath, '/')
    },
    // Donne le fichiers et la ligne du code qu'on inspecte ou quand il y a une erreur est désactivé quand il est pas en mode dev
    // lien doc: https://webpack.js.org/configuration/devtool/
    devtool: dev ? "cheap-module-eval-source-map" : false,
    // configuration pour le rechargement automatique apres modif du css, js et scss
    // lien doc: https://webpack.js.org/configuration/dev-server/
    devServer: {
        // Dire au serveur d'où le contenue vien
        // Il est conseillé d'avoir un chemin absolue donc on utilise path.resolve (une fonction nodeJs)
        // lien doc: https://webpack.js.org/configuration/dev-server/#devserver-contentbase
        contentBase: path.join(__dirname, './web'),
    },
    // config des modules webpack c'est eux qui vont faire des modifications / vérifivation / testé les fichiers en entré avant de les builder
    // lien doc: https://webpack.js.org/concepts/modules/
    module: {
        // définir les régles a respecté pour modifier / vérifier / tester
        // lien doc: https://webpack.js.org/configuration/module/#rule-conditions
        rules: [
            {
                // Option pour dire que cette regle doit etre appliqué en premier
                //lien doc: https://webpack.js.org/configuration/module/#rule-enforce
                enforce: 'pre',
                // Expression réguliere tous les fichiers qui finnisse en .js
                //lien doc: https://webpack.js.org/configuration/module/#rule-test
                test: /\.js$/,
                // Dossier exclu
                // lien doc: https://webpack.js.org/configuration/module/#rule-exclude
                exclude: /node_modules/,
                // Utilise eslint, eslint vérifie si le code js ne comporte pas d'erreur d'écriture ni d'erreur de style.
                // Il va regardé le .eslintrc.js pour savoir qu'elle regle on utilise. ici c'est les régles d'airbnb (https://github.com/airbnb/javascript)
                // Lien doc loader: https://webpack.js.org/loaders/eslint-loader/
                // Lien vers eslint doc: https://eslint.org/docs/rules/indent
                use: ['eslint-loader']
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: cssLoader
                }))
            },
            {
                test: /\.scss$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [...cssLoader,
                        'sass-loader'
                    ]
                }))
            },
            {
                test: /\.(png|jpg|gif|woff|woff2|eot|ttf|otf)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 8192,
                        name: '[name].[hash].[ext]'
                    },
                },
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.[hash].css'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'front/images/sprite/common'),
                glob: '*.png'
            },
            target: {
                image: path.resolve(__dirname, 'front/images/sprite.png'),
                css: path.resolve(__dirname, 'front/scss/_sprite.scss')
            },
            retina: '@2x',
            apiOptions: {
                cssImageRef: path.resolve(__dirname, 'front/images/sprite.png')
            },
        }),
        new webpack.HotModuleReplacementPlugin(),
        new StyleLintPlugin({
            configFile: '.stylelintrc',
            context: './front/scss/',
            syntax: 'scss',
            emitErrors: false
        }),
        new BrowserSyncPlugin(
            // BrowserSync options
            {
                // browse to http://localhost:3000/ during development
                host: 'localhost',
                port: 3000,
                // proxy the Webpack Dev Server endpoint
                // (which should be serving on http://localhost:3100/)
                // through BrowserSync
                proxy: 'http://localhost:3100/'
            },
            // plugin options
            {
                // prevent BrowserSync from reloading the page
                // and let Webpack Dev Server take care of this
                reload: false
            }
        ),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(__dirname, entryThemePath),
        }),
    ]
};

if (!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: false,
    }));
    config.plugins.push(new CleanWebpackPlugin(
        [entryThemePath],
        {
            root: __dirname,
            dry: false,
            watch: true,
        }
    ));

    config.plugins.push(
        new ImageminPlugin({
            test: path.resolve(__dirname, 'assets/images'),
            quality: '95'
        })
    )
}

module.exports = config;
